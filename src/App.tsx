import { Hat } from './components/Hat';
import { Form } from './components/Form';
import { CssBaseline } from '@mui/material';

// work time:
// from 17:00 until 20:00 // 3h // setup project and creating validation rules
// from 12:30 until 16:00 // 3.5h // create some components and pacient data form
// from 22:30 until 01:00 // 1.5h // create all validations and performance optimization

/**
 * есть куча чего еще доделать и улучшить:
 * - доделать датапикер, что бы он внятно работал
 * - обмазать объект юзера номинальными типами
 * - можно дописать валидации номера для каждого типа документа
 * - тогл на инпуте я не придумал как быстро сделать, отложил на потом, а потом забыл) в час ночи ковырять это уже не хочу
 * - страну и место ( если это город ) можно проверять на существование
 * - причесать маску на телефоне, сейчас туда нельзя вставить телефон, который предлагает хром
 * - причесать валидации, выглядят не очень красиво
 */

function App() {
  return (
    <div>
        <CssBaseline />
        <Hat />
        <Form />
    </div>
  );
}

export default App;
