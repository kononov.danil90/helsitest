import { useField } from "react-final-form"
import { DatePicker } from "../DatePicker"
import { genericMemo } from "../../helpers/functions";

interface IProps<T extends Record<string, unknown>> {
  name: keyof T;
  label: string;
  isRequired?: boolean;
  minDate?: Date;
  maxDate?: Date;
}

const FinalDatePickerComponent = <T extends Record<string, unknown>>(p: IProps<T>) => {
  const {
    name,
    label,
    isRequired,
    minDate,
    maxDate,
  } = p;

  const {
    input,
    meta
  } = useField(name as string);

  return (
    <DatePicker
      name={name as string}
      label={label}
      isRequired={isRequired}
      onChange={input.onChange}
      value={input.value}
      isError={Boolean(meta.error)}
      isTouched={meta.touched}
      helperText={meta.touched && Boolean(meta.error) ? meta.error : undefined}
      minDate={minDate}
      maxDate={maxDate}
    />
  )
}

export const FinalDatePicker = genericMemo(FinalDatePickerComponent);