import React, { memo } from 'react';
import { IInputProps, Input } from '../Input';
import { IMaskInput } from 'react-imask';

export interface IMaskedInputProps extends IInputProps {
  maskConfig: Record<string, unknown>;
}

export const MaskedInput: React.FC<IMaskedInputProps> = memo((props) => {
  const { InputProps, maskConfig, ...rest } = props;

  return (
    <Input
      variant='standard'
      {...rest}
      InputProps={{
        ...InputProps,
        /** @ts-ignore */
        inputComponent: IMaskInput,
        inputProps: {
          ...InputProps?.inputProps,
          ...maskConfig,
        },
      }}
    />
  );
});
