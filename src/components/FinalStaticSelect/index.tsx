import { useField } from "react-final-form";
import { IOption } from "../../helpers/types";
import { StaticSelect } from "../StaticSelect"
import { genericMemo } from "../../helpers/functions";

interface IProps<T extends Record<string, unknown>> {
  name: keyof T;
  label: string;
  isRequired?: boolean;
  options: IOption[];
}

export const FinalStaticSelectComponent = <T extends Record<string, unknown>>(p: IProps<T>) => {
  const {
    label,
    name,
    options,
    isRequired
  } = p;

  const {
    input,
    meta
  } = useField(name as string);

  return (
    <StaticSelect
      label={label}
      name={name as string}
      onChange={input.onChange}
      options={options}
      value={input.value}
      isRequired={isRequired}
      helperText={meta.touched && Boolean(meta.error) && meta.error}
      isError={Boolean(meta.error)}
      isTouched={meta.touched}
    />
  )
}

export const FinalStaticSelect = genericMemo(FinalStaticSelectComponent);