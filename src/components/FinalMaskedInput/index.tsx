import { useField } from "react-final-form";
import { MaskedInput } from "../MaskedInput"
import { genericMemo, getRequiredLabel } from "../../helpers/functions";

interface IProps<T extends Record<string, unknown>> {
  name: keyof T;
  label: string;
  helperText?: string;
  placeholder?: string;
  isRequired?: boolean;
  maskConfig: Partial<Record<'mask' | 'definitions', unknown>>;
}

const FinalMaskedInputComponent = <T extends Record<string, unknown>>(p: IProps<T>) => {
  const {
    label,
    name,
    helperText,
    placeholder,
    isRequired = false,
    maskConfig
  } = p;

  const {
    input,
    meta
  } = useField(name as string);

  return (
    <MaskedInput
      label={getRequiredLabel({ isRequired, label })}
      name={name as string}
      onChange={input.onChange}
      value={input.value}
      placeholder={placeholder}
      helperText={(meta.touched && Boolean(meta.error) && meta.error) || helperText}
      isError={Boolean(meta.error)}
      isTouched={meta.touched}
      maskConfig={maskConfig}
    />
  )
}

export const FinalMaskedInput = genericMemo(FinalMaskedInputComponent);