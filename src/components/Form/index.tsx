import styled from "@emotion/styled"
import { Container } from "../Styled"
import { Form as FForm } from "react-final-form";
import { useCallback, useState } from "react";
import { UserFormData, initialValues, validate } from "./schema";
import { FormApi } from "final-form";
import { FinalInput } from "../FinalInput";
import { FinalDatePicker } from "../FinalDatePicker";
import { FinalStaticSelect } from "../FinalStaticSelect";
import { FinalMaskedInput } from "../FinalMaskedInput";
import { VALIDATION_REGEXP } from "../../helpers/validation";

const Root = styled.section`
  padding: 20px 0;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  gap: 20px;
  margin-bottom: 30px;

  & > div {
    flex-grow: 1;
  }
`;

const DocumentTypeToMaskConfigMap: Record<Exclude<UserFormData['documentType'], ''>, Partial<Record<'mask' | 'definitions', unknown>>> = {
  idCard: {
    mask: '000000000'
  },
  passport: {
    mask: '##000000',
    definitions: {
      '#': VALIDATION_REGEXP.onlyUkLetters
    }
  },

  idCardWithProtection: {
    mask: '###00000',
    definitions: {
      '#': VALIDATION_REGEXP.onlyUkLetters
    }
  },
  permanentResidence: {
    mask: '###00000',
    definitions: {
      '#': VALIDATION_REGEXP.onlyUkLetters
    }
  },
  migrantCertificate: {
    mask: '###00000',
    definitions: {
      '#': VALIDATION_REGEXP.onlyUkLetters
    }
  },
  proofOfResidence: {
    mask: '###00000',
    definitions: {
      '#': VALIDATION_REGEXP.onlyUkLetters
    }
  },
  temporaryCertificate: {
    mask: '###00000',
    definitions: {
      '#': VALIDATION_REGEXP.onlyUkLetters
    }
  },
}

const DocumentTypeToPlaceholderMap: Record<Exclude<UserFormData['documentType'], ''>, string> = {
  idCard: '000000000',
  passport: 'АА000000',

  idCardWithProtection: 'ААА00000',
  permanentResidence: 'ААА00000',
  migrantCertificate: 'ААА00000',
  proofOfResidence: 'ААА00000',
  temporaryCertificate: 'ААА00000',
}

const TAX_INDEX_NUMBER_MASK_CONFIG = {
  mask: '0000000000'
}

const BIRTHDAY_MIN_DATE = new Date('01.01.1900');
const BIRTHDAY_MAX_DATE = new Date();

const STATIC_SELECT_SEX_OPTIONS = [
  {
    label: 'Male',
    value: 'male'
  },
  {
    label: 'Female',
    value: 'female'
  },
];

const STATIC_SELECT_PREFERRED_CONTACT_METHOD_OPTIONS = [
  {
    label: 'Електронною поштою',
    value: 'email'
  },
  {
    label: 'Телефоном',
    value: 'phone'
  },
];

const PHONE_MASK_CONFIG = {
  mask: '+38 (000) 000-00-00',
};

const STATIC_SELECT_DOCUMENT_TYPE_OPTIONS = [
  {
    label: 'Посвідчення особи, яка потреьує додаткового захисту',
    value: 'idCardWithProtection'
  },
  {
    label: 'Паспорт (ID-картка)',
    value: 'idCard'
  },
  {
    label: 'Паспорт (книжечка)',
    value: 'passport'
  },
  {
    label: 'Посвідка на постійне проживання в Україні',
    value: 'permanentResidence'
  },
  {
    label: 'Посвідка біженця',
    value: 'migrantCertificate'
  },
  {
    label: 'Посвідка на проживання',
    value: 'proofOfResidence'
  },
  {
    label: 'Тимчасове посвідчення громадянина України',
    value: 'temporaryCertificate'
  },
];


const DOCUMENT_WHEN_ISSUED_MIN_DATE = new Date('01.01.1900');
const DOCUMENT_WHEN_ISSUED_MAX_DATE = new Date();

const DOCUMENT_VALID_UNTIL_MIN_DATE = new Date();
const DOCUMENT_VALID_UNTIL_MAX_DATE = new Date('01.01.2100');

const EMPTY_MASK_CONFIG = {
  mask: '',
};

const DOCUMENT_RECORD_NUMBER_MASK_CONFIT = {
  mask: '00000000-00000'
};

export const Form = () => {
  const [result, setResult] = useState('');

  const onSubmit = useCallback((
    values: UserFormData,
    form: FormApi<UserFormData, Partial<UserFormData>>,
  ) => {
    void form;

    //@ts-ignore
    setResult(JSON.stringify(values, 0, 2));
  }, []);

  return (
    <Root>
      <Container>
        <Row>
          <h2>
            Данні пацієнта
          </h2>
        </Row>
        <FForm<UserFormData>
          onSubmit={onSubmit}
          initialValues={initialValues}
          validate={validate}
        >
          {({ handleSubmit, values }) => {

            return (
              <form onSubmit={handleSubmit}>
                <Row>
                  <FinalInput<UserFormData>
                    name="surrname"
                    label='Прізвище'
                    isRequired
                  />
                  <FinalInput<UserFormData>
                    name="name"
                    label="Ім'я"
                    isRequired
                  />
                  <FinalInput<UserFormData>
                    name="patronymic"
                    label='По батькові'
                    isRequired
                    isOptional
                  />
                </Row>
                <Row>
                  <FinalMaskedInput<UserFormData>
                    name="tin"
                    label="РНОКПП (ІПН)"
                    maskConfig={TAX_INDEX_NUMBER_MASK_CONFIG}
                    isRequired
                  />
                  <FinalDatePicker<UserFormData>
                    name="birthday"
                    label="Дата народження"
                    isRequired
                    minDate={BIRTHDAY_MIN_DATE}
                    maxDate={BIRTHDAY_MAX_DATE}
                  />
                  <FinalStaticSelect<UserFormData>
                    name="sex"
                    label="Стать"
                    isRequired
                    options={STATIC_SELECT_SEX_OPTIONS}
                  />
                </Row>
                <Row>
                  <FinalInput<UserFormData>
                    name="countryOfBirth"
                    label='Країна народження'
                    isRequired
                  />
                  <FinalInput<UserFormData>
                    name="placeOfBirth"
                    label='Місце народження'
                    isRequired
                  />
                </Row>
                <Row>
                  <FinalStaticSelect<UserFormData>
                    name="preferredContactMethod"
                    label="Бажаний спосіб зв'язку із пацієнтом"
                    options={STATIC_SELECT_PREFERRED_CONTACT_METHOD_OPTIONS}
                  />
                  <FinalInput<UserFormData>
                    name="secretWord"
                    label='Секретне слово (не менше 6 символів)'
                    isRequired
                  />
                </Row>
                <Row>
                  <FinalMaskedInput<UserFormData>
                    name="phone"
                    label='Контактний номер телефону'
                    maskConfig={PHONE_MASK_CONFIG}
                  />
                  <FinalInput<UserFormData>
                    name="email"
                    label='Адреса електронної пошти'
                  />
                </Row>
                <Row>
                  <h2>
                    Документ, що посвідчує особу
                  </h2>
                </Row>
                <Row>
                  <FinalStaticSelect<UserFormData>
                    name="documentType"
                    label='Тип документу'
                    options={STATIC_SELECT_DOCUMENT_TYPE_OPTIONS}
                    isRequired
                  />
                  <FinalMaskedInput<UserFormData>
                    name="documentNumber"
                    label='Серія (за наявності), номер'
                    maskConfig={values.documentType ? DocumentTypeToMaskConfigMap[values.documentType] : EMPTY_MASK_CONFIG}
                    placeholder={values.documentType ? DocumentTypeToPlaceholderMap[values.documentType] : ''}
                    isRequired
                  />
                </Row>
                <Row>
                  <FinalDatePicker<UserFormData>
                    name="documentWhenIssued"
                    label="Коли видано"
                    isRequired
                    minDate={DOCUMENT_WHEN_ISSUED_MIN_DATE}
                    maxDate={DOCUMENT_WHEN_ISSUED_MAX_DATE}
                  />
                  <FinalDatePicker<UserFormData>
                    name="documentValidUntil"
                    label="Діє до"
                    isRequired
                    minDate={DOCUMENT_VALID_UNTIL_MIN_DATE}
                    maxDate={DOCUMENT_VALID_UNTIL_MAX_DATE}
                  />
                </Row>
                <Row>
                  <FinalInput<UserFormData>
                    name="documentIssuedBy"
                    label='Ким видано'
                    isRequired
                  />
                  <FinalMaskedInput<UserFormData>
                    name="documentRecordNumber"
                    label='Запис №(УНЗР)'
                    maskConfig={DOCUMENT_RECORD_NUMBER_MASK_CONFIT}
                    placeholder='00000000-00000'
                    isRequired
                    helperText='Вкажіть унікальниій номер запису в Демографічному рєєстрі (Запис №)'
                  />
                </Row>
                <button type="submit">Submit</button>
              </form>
            )
          }}
        </FForm>
        <pre>{result}</pre>
      </Container>
    </Root>
  )
}