import * as Yup from 'yup';
import { GenericConstraints } from '../../helpers/types';
import { validateForm } from '../../helpers/functions';
import { VALIDATION_ERRORS, VALIDATION_REGEXP, YUP_VALIDATIONS } from '../../helpers/validation';

export type UserFormData = {
  surrname: string;
  name: string;
  patronymic: string;
  tin: string;
  birthday: Date | null;
  sex: string;
  countryOfBirth: string;
  placeOfBirth: string;
  preferredContactMethod: 'email' | 'phone' | '';
  secretWord: string;
  phone: string;
  email: string;

  documentType:
  | ''
  | 'idCardWithProtection'
  | 'idCard'
  | 'passport'
  | 'permanentResidence'
  | 'migrantCertificate'
  | 'proofOfResidence'
  | 'temporaryCertificate';
  documentNumber: string;
  documentWhenIssued: Date | null;
  documentValidUntil: Date | null;
  documentIssuedBy: string;
  documentRecordNumber: string;
}

export const initialValues: UserFormData = {
  surrname: '',
  name: '',
  patronymic: '',
  tin: '',
  birthday: null,
  sex: '',
  countryOfBirth: '',
  placeOfBirth: '',
  preferredContactMethod: '',
  secretWord: '',
  phone: '',
  email: '',

  documentType: '',
  documentNumber: '',
  documentWhenIssued: null,
  documentValidUntil: null,
  documentIssuedBy: '',
  documentRecordNumber: '',
}

export const Constraints: GenericConstraints<UserFormData> = () => ({
  surrname: YUP_VALIDATIONS.requiredDefaultString(),
  name: YUP_VALIDATIONS.requiredDefaultString(),
  patronymic: YUP_VALIDATIONS.requiredDefaultString().nullable(),
  tin: YUP_VALIDATIONS.tin().nullable(),
  birthday: Yup.date().required(VALIDATION_ERRORS.required).min(new Date('01.01.1900'), VALIDATION_ERRORS.minDate).max(new Date(), VALIDATION_ERRORS.maxDate),
  sex: YUP_VALIDATIONS.sex(),
  // можно проверять, существует ли такая страна
  countryOfBirth: YUP_VALIDATIONS.requiredDefaultString(),
  // можно проверять, существует ли такой город ( если это город, а не просто адрес ) в такой стране
  placeOfBirth: YUP_VALIDATIONS.requiredString(),
  preferredContactMethod: YUP_VALIDATIONS.preferredContactMethod(),
  secretWord: YUP_VALIDATIONS.secretWord(),
  phone: YUP_VALIDATIONS.string().test((value, { createError, parent }) => {
    const {
      preferredContactMethod
    } = parent as UserFormData;

    if (preferredContactMethod === 'phone') {
      if (!value) {
        return createError({ message: VALIDATION_ERRORS.required })
      }

      if (!value.match(VALIDATION_REGEXP.phone)) {
        return createError({ message: VALIDATION_ERRORS.wrongPhone })
      }
    }

    return true;
  }),
  email: YUP_VALIDATIONS.string().test((value, { createError, parent }) => {
    const {
      preferredContactMethod
    } = parent as UserFormData;

    if (preferredContactMethod === 'email') {
      if (!value) {
        return createError({ message: VALIDATION_ERRORS.required })
      }

      if (!value.match(VALIDATION_REGEXP.email)) {
        return createError({ message: VALIDATION_ERRORS.wrongEmail })
      }
    }

    return true;
  }),
  documentType: Yup.string()
    .required(VALIDATION_ERRORS.required)
    .oneOf([
      'idCardWithProtection',
      'idCard',
      'passport',
      'permanentResidence',
      'migrantCertificate',
      'proofOfResidence',
      'temporaryCertificate'
    ], 'Wrong document type'),
  documentNumber: YUP_VALIDATIONS.string()
    .required(VALIDATION_ERRORS.required)
    .test((value, { createError, parent }) => {
      const {
        documentType
      } = parent as UserFormData;

      if (documentType === 'idCard') {
        if (!value.match(VALIDATION_REGEXP.idCard)) {
          return createError({ message: VALIDATION_ERRORS.idCard });
        }
      } else if (documentType === 'passport') {
        if (!value.match(VALIDATION_REGEXP.passport)) {
          return createError({ message: VALIDATION_ERRORS.passport })
        }
        // дальше можно так же перебирать все остальные варианты
      } else if (!value.match(VALIDATION_REGEXP.documentNumber)) {
        return createError({ message: VALIDATION_ERRORS.documentNumber })
      }

      return true
    }),
  documentWhenIssued: Yup.date().required(VALIDATION_ERRORS.required).min(new Date('01.01.1900'), VALIDATION_ERRORS.minDate).max(new Date(), VALIDATION_ERRORS.maxDate),
  documentValidUntil: Yup.date().required(VALIDATION_ERRORS.required).min(new Date(), VALIDATION_ERRORS.minDate).max(new Date('01.01.2100'), VALIDATION_ERRORS.maxDate),
  documentIssuedBy: YUP_VALIDATIONS.requiredDefaultString(),
  documentRecordNumber: YUP_VALIDATIONS.recordNumber(),
});

export const validationSchema = Yup.object().shape(Constraints());

export const validate = validateForm(validationSchema);
