import { TextField, TextFieldProps } from "@mui/material"
import { memo, useCallback } from "react";

export interface IInputProps {
  value: string;
  label: string;
  name: string;
  onChange(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>): void;
  placeholder?: string;
  helperText?: string;
  onBlur?(event: React.FocusEvent<HTMLTextAreaElement | HTMLInputElement>): void;
  onFocus?(event: React.FocusEvent<HTMLTextAreaElement | HTMLInputElement>): void;
  variant?: TextFieldProps['variant'];
  InputProps?: TextFieldProps['InputProps'];
  isError?: boolean;
  isTouched?: boolean;
  isRequired?: boolean;
  isDisabled?: boolean;
}

export const Input = memo((p: IInputProps) => {
  const {
    onChange,
    isError = false,
    isTouched = false,
    isRequired = false,
    isDisabled = false,
    ...rest
  } = p;

  const handleChange = useCallback((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    onChange(e);
  }, [onChange]);

  return (
    <TextField
      onChange={handleChange}
      error={isTouched && isError}
      required={isRequired}
      disabled={isDisabled}
      {...rest}
    />
  )
})