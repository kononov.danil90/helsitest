import { FormControl, FormHelperText, InputLabel, MenuItem, Select, SelectChangeEvent } from "@mui/material"
import { getRequiredLabel } from "../../helpers/functions";
import { memo, useCallback } from "react";
import { IOption } from "../../helpers/types";


interface IProps {
  options: IOption[];
  value: IOption['value'];
  onChange(v: IOption['value']): void;
  label: string;
  name: string;
  isRequired?: boolean;
  helperText?: string;
  isError?: boolean;
  isTouched?: boolean;
}

export const StaticSelect = memo((p: IProps) => {
  const {
    onChange,
    options,
    value,
    isRequired = false,
    label,
    name,
    helperText,
    isError = false,
    isTouched = false,
  } = p;

  const handleChange = useCallback((e: SelectChangeEvent<IOption>) => {
    onChange(e.target.value as string);
  }, [onChange])

  return (
    <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }} error={isTouched && isError}>
      <InputLabel id={`${label}-label`}>{getRequiredLabel({ isRequired, label })}</InputLabel>
      <Select
        labelId={`${label}-label`}
        value={options.find(({ value: v }) => v === value)!}
        label={getRequiredLabel({ isRequired, label })}
        onChange={handleChange}
        name={name}
      >
        <MenuItem value=''>
          --Select--
        </MenuItem>
        {options.map(({ label, value }) => (
          <MenuItem key={value} value={value}>{label}</MenuItem>
        ))}
      </Select>
      <FormHelperText>
        {helperText}
      </FormHelperText>
    </FormControl>
  )
})