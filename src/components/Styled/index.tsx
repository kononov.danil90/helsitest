import styled from "@emotion/styled";

export const Container = styled.div`
  max-width: 1236px;
  padding-left: 20px;
  padding-right: 20px;
  margin: 0 auto;
  width: 100%;
`;