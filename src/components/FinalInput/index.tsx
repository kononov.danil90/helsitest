import { Field } from "react-final-form"
import { Input } from "../Input"
import { Switch } from "@mui/material";
import { genericMemo, getRequiredLabel } from "../../helpers/functions";

interface IProps<T extends Record<string, unknown>> {
  name: keyof T;
  label: string;
  placeholder?: string;
  isRequired?: boolean;
  isOptional?: boolean;
}

const FinalInputComponent = <T extends Record<string, unknown>>(p: IProps<T>) => {
  const {
    name,
    label,
    placeholder,
    isRequired = false,
    isOptional = false,
  } = p;

  void [Switch, isOptional];

  return (
    <Field name={name as string}>
      {({ input, meta }) => {

        return (
          <Input
            {...input}
            label={getRequiredLabel({isRequired, label})}
            helperText={meta.touched && Boolean(meta.error) ? meta.error : undefined}
            isError={Boolean(meta.error)}
            isTouched={meta.touched}
            placeholder={placeholder}
            variant="standard"
            isDisabled={input.value === null}
            // InputProps={{
            //   endAdornment: isOptional ? (() => {
            //     const X = () => {

            //       return (
            //         <Switch
            //           onChange={(e) => {
            //             if (!e.target.checked) {

            //             }
            //           }}
            //         />
            //       )
            //     }

            //     return <X />
            //   })() : null
            // }}
          />
        )
      }}
    </Field>
  )
}

export const FinalInput = genericMemo(FinalInputComponent);