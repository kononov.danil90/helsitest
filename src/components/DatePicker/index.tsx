import { DatePicker as MuiDatePicker } from "@mui/x-date-pickers"
import { memo, useCallback } from "react";
import { getRequiredLabel } from "../../helpers/functions";

interface IProps {
  name: string;
  label: string;
  helperText?: string;
  value: Date | null;
  onChange(v: Date): void;
  isRequired?: boolean;
  isError?: boolean;
  isTouched?: boolean;
  minDate?: Date;
  maxDate?: Date;
}

export const DatePicker = memo((p: IProps) => {
  const {
    label,
    value,
    helperText = '',
    onChange,
    isError = false,
    isRequired = false,
    isTouched = false,
    minDate,
    maxDate,
  } = p;

  const handleChange = useCallback((v: any) => {
    onChange(v as Date);
  }, [onChange]);

  return (
    <MuiDatePicker
      minDate={minDate}
      maxDate={maxDate}
      label={getRequiredLabel({ isRequired, label })}
      value={value}
      onChange={handleChange}
      slotProps={{
        textField: {
          helperText: isTouched && isError && helperText,
          error: isTouched && isError,
          variant: 'standard',
        }
      }}
    />
  )
})