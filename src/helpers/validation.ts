import * as Yup from 'yup';

export const VALIDATION_REGEXP = {
  phone: /\+38 \(\d{3}\) \d{3}-\d{2}-\d{2}/,
  email: /^\w+(?:[.\-+]?\w+)*@\w+(?:[.\-]?\w+)*(?:\.\w{2,63})+$/,
  recordNumber: /\d{8}-\d{5}/,
  idCard: /\d{9}/,
  passport: /[а-щА-ЩЬьЮюЯяЇїІіЄєҐґ]{2}\d{6}/,
  documentNumber: /[а-щА-ЩЬьЮюЯяЇїІіЄєҐґ]{3}\d{5,9}/,
  onlyUkLetters: /[а-щА-ЩЬьЮюЯяЇїІіЄєҐґ]/,
}

export const VALIDATION_ERRORS = {
  required: 'Field is required',
  wrongPhone: 'Wrong phone number. Example: +380 (093) 999-88-77',
  wrongEmail: 'Wrong email. Example: some.email@gmail.com',
  minDate: 'Min date error',
  maxDate: 'Max Date error',
  idCard: 'Wrong id card number',
  passport: 'Wrong passport number',
  documentNumber: 'Wrong document number',
}

export const YUP_VALIDATIONS = {
  string: () => Yup.string(),
  requiredString: () => Yup.string().required(VALIDATION_ERRORS.required),
  requiredDefaultString: () => YUP_VALIDATIONS.requiredString()
    .min(2, 'Minimum length is 2')
    .max(30, 'Maximum length is 30'),
  tin: () => YUP_VALIDATIONS.requiredString()
    .length(10, 'Length must be 10'),
  sex: () => YUP_VALIDATIONS.requiredString()
    .oneOf(['male', 'female'], 'Wrong sex'),
  preferredContactMethod: () => YUP_VALIDATIONS.string()
    .oneOf(['phone', 'email', ''], 'Wrong contact type'),
  secretWord: () => YUP_VALIDATIONS.requiredString()
    .min(6, 'Minimum length is 6')
    .max(30, 'Maximum length is 30'),
  recordNumber: () => YUP_VALIDATIONS.requiredString()
    .test((value, { createError }) => {

      if (!value.match(VALIDATION_REGEXP.recordNumber)) {
        return createError({ message: 'Wrong record number' })
      }

      return true;
    })
}
