export type GenericConstraints<T extends Record<string, unknown>> = () => Record<keyof T, any>;

export type GenericErrors<T extends Record<string, any>> = {
  [K in keyof T]?: string;
}

export type IOption = {
  label: string;
  value: string;
}

export type ValueOf<T> = T[keyof T];