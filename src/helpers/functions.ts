import { setIn } from 'final-form';
import * as Yup from 'yup';
import { GenericErrors } from './types';
import { memo } from 'react';

export const validateForm = <T extends ReturnType<typeof Yup.object>>(schema: T) => async <V extends Record<string, unknown>>(values: V): Promise<GenericErrors<V> | undefined> => {
  try {
    await schema.validate(values, { abortEarly: false });

    return undefined;
  } catch (e) {
    const err = e as any;

    const errors = err.inner.reduce((acc: Record<string, string>, innerError: { message: string, path: string }) => {
      return setIn(acc, innerError.path, innerError.message);
    }, {});

    return errors;
  }
}


export const getRequiredLabel = ({
  isRequired,
  label
}: {
  isRequired: boolean,
  label: string;
}) => isRequired ? `${label} *` : label;

export const genericMemo = <T>(C: T): T => {

  //@ts-ignore
  return memo(C) as T
}